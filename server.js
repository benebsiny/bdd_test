const Koa = require('koa')
const fs = require('fs')
const app = new Koa()
app.use((ctx, next) => {
    ctx.type = 'html'
    ctx.body = fs.createReadStream('src/web/index.html')
})
app.listen(3000)