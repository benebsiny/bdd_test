const fs = require('fs');
const readline = require('readline');

let sentences = [];

// 輸出step檔所需元件
function init() {
    output += "const { Given, When, Then } = require('cucumber');\r\n";
    output += "const expect = require('chai').expect;\r\n\r\n";
}

// 去除前後方空白
function trim(sentence) {
    return sentence.replace(/^\s+|\s+$/g, "");
}

// 計算變數數量
function sentenceVariableCount(sentence) {
    return (sentence.match(/"[^"]+"|<[^<>]+>|[\d]+/g))?sentence.match(/"[^"]+"|<[^<>]+>|[\d]+/g).length: 0;
}

// 變數取代
function sentenceReplace(sentence) {
    sentence = sentence.replace(/"[^"]+"|"<[^<>]+>"/g, '{string}'); // 字串 & 用於Scenario Outline的字串
    sentence = sentence.replace(/[\d]|<[^<>]+>/g, '{int}'); // 整數
    return sentence;
}

// 檢查是否輸出過該敘述
function isRepeat(sentence) {
    if (~sentences.indexOf(sentence)) return true;
    return false;
}

function generateOutput(mode, sentence, variableCount) {
    
    // 檢查該句子是否已經輸出過
    if (isRepeat(sentence)) return;
    sentences.push(sentence);

    output += `${mode}('${sentence}', function (`;

    for (let i = 1; i <= variableCount; i++){
        output += `var${i}`;
        if (i !== variableCount) output += ', ';
    }
    
    output += `) {\r\n\r\n});\r\n\r\n`;
}


const data = fs.readFileSync('./src/test/features/test.feature', 'utf8');
const line = data.split('\r\n');
let output = '';

init();

line.forEach(data => {
    data = trim(data);
    const word = data.split(' ')

    if (word[0].match(/(G|g)iven/)) { // Given

        // 抓Given後面的句子
        let sentence = data.match(/(G|g)iven?(.*)/)[2];
        const variableCount = sentenceVariableCount(sentence);

        // 變數取代
        sentence = sentenceReplace(sentence);
        sentence = trim(sentence)
        
        generateOutput('Given', sentence, variableCount);
    }
    else if (word[0].match(/(W|w)hen/)) { // When
        let sentence = data.match(/(W|w)hen?(.*)/)[2];
        const variableCount = sentenceVariableCount(sentence);

        sentence = sentenceReplace(sentence);
        sentence = trim(sentence)
        
        generateOutput('When', sentence, variableCount);
    }
    else if (word[0].match(/(T|t)hen/)) { // Then
        let sentence = data.match(/(T|t)hen?(.*)/)[2];
        const variableCount = sentenceVariableCount(sentence);

        sentence = sentenceReplace(sentence);
        sentence = trim(sentence);

        generateOutput('Then', sentence, variableCount);
    }
});

// 檔案已經存在
if (fs.existsSync('./src/test/features/steps.js')) {
    var rl = readline.createInterface(process.stdin, process.stdout);
    rl.setPrompt('The file has already exists, would you like to replace it?(Y/N)');
    rl.prompt();
    rl.on('line', function (answer) {
        // 回答yes
        if (answer.match(/(y|Y)(es)?/)) {
            fs.writeFileSync('./src/test/features/steps.js', output, err => {
                if (err)
                    console.log(err);
            });
            console.log('Process complete');
            rl.close();
        }
        // 回答no
        else if (answer.match(/(n|N)(o)?/)) { 
            console.log('Process canceled');
            rl.close();
        }
        rl.prompt(); // 隨便回答
    })
    .on('close', function () {
        process.exit(0);
    });
}
else {
    fs.writeFileSync('./src/test/features/steps.js', output, err => {
        if (err)
            console.log(err);
    });
    console.log('Process complete');
}