const expect = require('chai').expect
const { Given, When, Then } = require('cucumber')

Given('轉換模式是{string}', function (var1) {
    $(`//option[. ='${var1}']`).click()
})

When('我更改轉換模式至{string}', function (var1) {
    $(`//option[. ='${var1}']`).click()
})

Then('我可以在轉換模式選擇欄的下面看見{string}', function (var1) {
    let label = $('#label-temp').getText()
    expect(label).to.equal(var1)
})


Given('溫度的輸入欄是空的', function () {
    $('#input-temp').setValue('')
})

When('我按下了轉換按鈕', function () {
    $('.btn.btn-primary').click()
})

Then('我可以在溫度輸入欄的下方看見{string}', function (var1) {
    let errmsg = $('#error-msg').getText()
    expect(errmsg).to.equal(var1)
})


When('我輸入了{int}然後按下轉換按鈕', function (var1) {
    $('#input-temp').setValue(var1)
    $('.btn.btn-primary').click()
})

Then('我可以看到轉換結果{string}', function (var1) {
    let result = $('h3').getText()
    expect(result).to.equal(var1)
})
